### Week 6 notes
----------------

**Assembler** is program that transform assembly language to binary code

#### Basic assembler Logic
Repeat

  - read the next assembly language command
  - break it into different fields it is composed of
  - lookup the binary code for each field
  - combine those codes into a single maschine language command
  - output the machine language command

Until EOF


##### Symbols:
  - can be:
    - labels (JMP _loop_)
    - variables (Load, R1, _weight_)

  - _label_ and _weight_ will be translated automatically into **addresses**
  - assembler has to figure out what address to assign to each symbol
  - have a map<_symbol_, _address_>
  - when the assembler finds a symbol, it will lookup in the table to find it; if it is there, it will just use that address, otherwise it will lookup for the first unallocated memory space and will add the symbol to the table together with its address

  - **forward reference**
    - appears when we encounter a jump to a _label_ that was not previously defined in the program
    - possible solutions:
      - leave blank address in the map until _label_ appears and then fix it (a little bit harder because it supposes to fix all the lines that were previously dependent on the _label_)
      - pass the file two times: first time to find _labels_, second time to parse the actual code

#### Hack language specification
Hack language consists of A-instructions, C-instructions and symbols

##### A-instruction
e.g.: @value, @21

Translate it into binary: 0000 0000 0001 0101

All A-instructions have the MSB = 0

##### C-instruction
Symbolic syntax: <span style="color:red">dest</span> = <span style="color:blue">comp</span>; <span style="color:green">jump</span>

Binary syntax: 1 1 1 <span style="color:blue">a c1 c2 c3 c4 c5 c6</span> <span style="color:red"> d1 d2 d3 </span> <span style="color:green"> j1 j2 j3

|computation|   |c1 |c2 |c3 |c4 |c5 |c6 |
|:---------:|:-:|:-:|:-:|:-:|:-:|:-:|:-:|
| a = 0 | a = 1 |   |   |   |   |   |   |
|   0   |       | 1 | 0 | 1 | 0 | 1 | 0 |
|   1   |       | 1 | 1 | 1 | 1 | 1 | 1 |
|   -1  |       | 1 | 1 | 1 | 0 | 1 | 0 |
|   D   |       | 0 | 0 | 1 | 1 | 0 | 0 |
|   A   |   M   | 1 | 1 | 0 | 0 | 0 | 0 |
|   !D  |       | 0 | 0 | 1 | 1 | 0 | 1 |
|   !A  |   !M  | 1 | 1 | 0 | 0 | 0 | 1 |
|   -D  |       | 0 | 0 | 1 | 1 | 1 | 1 |
|   -A  |   -M  | 1 | 1 | 0 | 0 | 1 | 1 |
|  D+1  |       | 0 | 1 | 1 | 1 | 1 | 1 |
|  A+1  |  M+1  | 1 | 1 | 0 | 1 | 1 | 1 |
|  D-1  |       | 0 | 0 | 1 | 1 | 1 | 0 |
|  A-1  |  M-1  | 1 | 1 | 0 | 0 | 1 | 0 |
|  D+A  |  D+M  | 0 | 0 | 0 | 0 | 1 | 0 |
|  D-A  |  D-M  | 0 | 1 | 0 | 0 | 1 | 1 |
|  A-D  |  M-D  | 0 | 0 | 0 | 1 | 1 | 1 |
|D and A|D and M| 0 | 0 | 0 | 0 | 0 | 0 |
|D or A |D or M | 0 | 1 | 0 | 1 | 0 | 1 |

|dest|d1 |d2 |d3 | effect |
|:--:|:-:|:-:|:-:|:------:|
|null| 0 | 0 | 0 | value not stored |
|  M | 0 | 0 | 1 | RAM[A] |
|  D | 0 | 1 | 0 | D register |
| MD | 0 | 1 | 1 | RAM[A] and D register |
|  A | 1 | 0 | 0 | A register |
| AM | 1 | 0 | 1 | A register and RAM[A] |
| AD | 1 | 1 | 0 | A register and D register |
| AMD| 1 | 1 | 1 | A register, RAM[A], D register |

|jump |j1 |j2 |j3 | effect |
|:---:|:-:|:-:|:-:|:------:|
|null | 0 | 0 | 0 | no jump |
| JGT | 0 | 0 | 1 | if out > 0 |
| JEQ | 0 | 1 | 0 | if out = 0 |
| JGE | 0 | 1 | 1 | if out >= 0 |
| JLT | 1 | 0 | 0 | if out < 0 |
| JNE | 1 | 0 | 1 | if out != 0 |
| JLE | 1 | 1 | 0 | if out <= 0 |
| JMP | 1 | 1 | 1 | unconditional jump |


##### Symbols

| symbol | value |
|:------:|:-----:|
|   R0   |   0   |
|   R1   |   1   |
|   ...  |  ...  |
|   R15  |   15  |
| SCREEN | 16384 |
|   KBD  | 24576 |
|   SP   |   0   |
|   LCL  |   1   |
|   ARG  |   2   |
|  THIS  |   3   |
|  THAT  |   4   |

Label declaration: (label)

Variable declaration: \@variable

##### Whitespaces
Whitespaces are ignored

Comments start with *//*


#### Translation

##### A-instruction
- translate the number into binary on 15 bits
- append 0 as the MSB of the 15 bits

##### C-instruction
- use tables above
- all C-instructions start with 111

##### Symbols
There are 3 types of symbols:
  - variable symbols (represent memory locations where the programmeer wants to maintain values)
  - label symbols (represent destinations of goto instructions)
  - pre-defined symbols (represent special memory location)

1. Predefined symbols
  - in hack language there are 23 pre-defined symbols (see table above)
  - @predefinedSymbol -> replace predefinedSymbol with its value from the table

2. Label symbols
  - (xxx) -> symbol xxx refers to the memory location holding the next instruction in the program
  - assign to each instruction a number and whenever you refer to @xxx you actually refer to @<instructionNumber>

3. Variable symbols
  - any symbol that appears in the program which is not predefined and is not defined elsewhere using (var) directive is treated as a variable
  - each variable is assigned a unique memory address starting at 16
  - if you see @var for the first time, assign a unique memory address and replace var with its value

Symbol table = a map<symbol, value>
  - create  a symbol table
  - populate it with predefined symbols
  - parse the code file and find labels (also count how many instructions you found so far)
  - add into symbol table the labels with their value being the instruction number
  - start to scan entire program to parse
  - whenever you find a label that is not in the symbol table consider it a variable and assign it a value

#### The assembly process
1. Initialization
  - construct an empty symbol table
  - add the predefined symbols to the table

2. First pass
  - scan the entire program
  - for each instruction of the form (xxx)
    - add the pair (xxx, address) to the symbol table (address is the number of instruction following (xxx))

3. Second pass
  - set n = 16
  - scan the entire program again
  - for each instruction:
    - if the instruction is @symbol look up the symbol in the table
      - if (symbol, value) is found, use value to complete the instruction's translation
      - else
        - add (symbol, n) to the symbol table
        - use n to complete the instruction's translation
        - n++
    - if the instruction is a C-instruction, complete the instruction's translation
    - write the translated instruction to the output file
