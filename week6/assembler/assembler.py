import argparse
from assembler_helper import *

parser = argparse.ArgumentParser(description="translate assembly into bytecode")
parser.add_argument("--input_file", type=str, help="input file name")
parser.add_argument("--output_file", type=str, help="output file name")

args = parser.parse_args()

if not args.input_file or not args.output_file:
    print("usage: python assembler.py --input_file <file_name> --output_file <file_name>")
    exit(0)

input_file = open(args.input_file, "r")
output_file = open(args.output_file, "w+")

code_lines = []
for line in input_file:
    code_lines.append(line)

code_lines = clean_code_lines(code_lines)

parsed_code = parse_code(code_lines)

for line in parsed_code:
    output_file.write(line)
    output_file.write("\n")

output_file.close()
