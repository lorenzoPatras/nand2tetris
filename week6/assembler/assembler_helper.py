from assembler_tables import *

available_memory_address = 16 # first available spot

def is_empty_line(line):
    return not line.strip()

def is_comment(line):
    return line.find("//") is 0

def is_label(line):
    return line[0] is '(' and line[-1] is ')'

def is_a_instr(line):
    return line[0] is '@' and ';' not in line

def remove_inline_comments(lines):
    clean_lines = []

    for line in lines:
        comment_idx = line.find("//")
        if comment_idx > 0:
            clean_lines.append(line[0:comment_idx].strip())
        else:
            clean_lines.append(line)

    return clean_lines


def clean_code_lines(lines):
    clean_lines = []

    for line in lines:
        if is_empty_line(line):
            continue

        if is_comment(line):
            continue

        clean_lines.append(line.strip())

    clean_lines = remove_inline_comments(clean_lines)
    return clean_lines

def parse_labels(lines):
    instruction_number = 0
    clean_lines = []

    for line in lines:
        if is_label(line):
            symbol_table[line[1:-1]] = instruction_number
        else:
            instruction_number += 1
            clean_lines.append(line)

    return clean_lines

def parse_a_instruction(line):
    global available_memory_address
    label = line[1:] # ignore '@'

    try: # @1, @2 etc
        value = int(label)
    except ValueError: #@label
        if label not in symbol_table:
            symbol_table[label] = available_memory_address
            available_memory_address += 1
        value = symbol_table[label]

    binary_instruction = bin(value)[2:].zfill(16)
    return binary_instruction

def parse_c_instruction(line):
    c_padding = "111"
    binary_jump = "000"
    binary_destination = "000"
    binary_instruction = "0000000"

    splits = line.split(";")
    if len(splits) is not 1: # contains jump instruction
        jump = splits[1]
        binary_jump = jump_table.get(jump, "000")

    operation = splits[0]
    splits = operation.split("=")

    if len(splits) is not 1: # contains destination
        binary_destination = destination_table.get(splits[0], "000")
        binary_instruction = computation_table.get(splits[1], "0000000")
    else: # only instruction
        binary_instruction = computation_table.get(splits[0], "0000000")

    return c_padding + binary_instruction + binary_destination + binary_jump

def parse_code(lines):
    binary_code = []
    clean_lines = parse_labels(lines);

    for code_line in clean_lines:
        if is_a_instr(code_line):
            binary_result = parse_a_instruction(code_line)
        else:
            binary_result = parse_c_instruction(code_line)

        binary_code.append(binary_result)

    return binary_code
