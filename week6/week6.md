# Week6
-------

### Request
Create an assembler that translates assembly code into machine code
- the source program is specified in a text file named xxx.asm
- the generated code is written into a text file named xxx.hack
- assume xxx.asm is error-free

There are two types of asm files
  - the ones with an L don't have symbols inside (create an assembler to handle these first)
  - the normal ones contain symbols
  
