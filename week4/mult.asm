// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.
// load R0 and R1 into n0 and n1
@R0
D = M
@n0
M = D

@R1
D = M
@n1
M = D

// create the result registry and initialize with 0
@res
M = 0

// create the multiplication loop
(MULTIPLICATION)

// check if n0 is 0 and jump to result
@n0
D = M
@RESULT
D; JEQ

// accumulate in res var n1
@n1
D = M
@res
M = M + D

// decrement n0
@n0
M = M - 1

// jump back to the begining
@MULTIPLICATION
0; JMP


// put result into R2
(RESULT)
@res
D = M
@R2
M = D


// infinite loop at the end of the program
(END)
@END
0; JMP
