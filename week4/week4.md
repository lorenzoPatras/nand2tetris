# Week4
-------

### Request:
1. Multiplication

Write the assembly code that computes the multiplication of registers R0 and R1 and stores the result in R2.

2. Fill screen

Write the assembly code that fills the screen of the computer with black pixels whenever a key is pressed and with white pixels when a key is released.
