// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed.
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.

// key listener

// find screen size (screen reg is the second to last one while keyboard register is the last one)
@KBD
D = A
@end_address
M = D - 1


// monitor key to change screen state depending on key value
// with this implementation the screen state will be changed every time
// possible improvement: add a temp variable to store the previous state of
// the keyboard and change screen state only if the keyboard has changed its state
(KEYBOARD_LOOP)
@KBD
D = M

@SCREEN_EMPTY
D; JEQ

@SCREEN_FILL
D; JNE


// routine to fill the screen
(SCREEN_FILL)
@i
M = 0

(FILL_LOOP)
// copy the end address in a temp value
@end_address
D = M
@temp_end
M = D

@i
D = M

// go incrementally from @SCREEN to @KBD register and set each register to 11111111 (-1)
@SCREEN
D = A
@i
D = D + M

A = D
M = -1

// increment i
@i
M = M + 1

// check if last screen register is reached
@temp_end
M = M - D
D = M

// jump back to fill loop if last screen register was not reached
@FILL_LOOP
D;JNE

// jump to kbd loop
@KEYBOARD_LOOP
0; JEQ


// routine to empty the screen
(SCREEN_EMPTY)
@i
M = 0

(EMPTY_LOOP)
// copy the end address in a temp value
@end_address
D = M
@temp_end
M = D

@i
D = M

// go incrementally from @SCREEN to @KBD register and set each register to 11111111 (-1)
@SCREEN
D = A
@i
D = D + M

A = D
M = 0

// increment i
@i
M = M + 1

// check if last screen register is reached
@temp_end
M = M - D
D = M

@EMPTY_LOOP
D;JNE

@KEYBOARD_LOOP
0; JEQ
