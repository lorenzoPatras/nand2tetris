### Week 3 request
------------------

Use a DFlip-Flop primitive.
DFlip-Flop is a sequential logic gate whose output is out(t) = in(t - 1).

1. Create a 1 bit register

1 bit register has an extra load bit which decides whether the in value will be stored on the chip or the chip will retain its inside value.

2. Create a 16 bit register

3. Create RAM memories of 8 - 16K registers memory

4. Create a Program Counter

Program counter acts like a register of 16 bits with 3 extra bits:
  - reset: resets internal state to 0
  - load: loads the input into the internal state
  - inc: increments current internal state
