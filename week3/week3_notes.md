### Week 3 notes
----------------

DFlip-Flop = sequential logic gate capable to hold input value until a clock signal is provided

1 bit register is basically a DFlip-Flop logic gate with an extra loading bit. When the load bit is set on '1' the register will load the input information
