### Program control

1. Branching commands:
  - goto _label_
  - if-goto _label_
  - label _label_

2. Function commands:
  - call _function_name_ _args_
  - function _function_name_ _args_
  - return

#### Branching

e.g.:
'''asm

label a
  command
  if condition goto b
  command
label b
  command
  if condition goto a

'''

1. Unconditional branching
  - goto label
  - jump to label and execute the commands after it

2. Conditional branching
  - if-goto label
  - push onto the stack the condition, then check the condition and jump

#### Functions

- if you want to call a function with _n_ arguments push the _n_ args on the stack and call the function
- during runtime each function uses a working stack + memory segments
- the working stack and some of the segments should be
  - created when the function starts running
  - maintained as long as the function is executing
  - recycled when the function returns

- function call: call _func_ _args_
  - 2 save the caller's frame (save return address, LCL, ARG, THIS, THAT)
  - set up the local segment of the called function by pushing the _args_
  - on return
    - copy the return value onto argument 0
    - restore the segment pointers to the caller
    - clear the stack
    - set SP for the caller
    - jump to the return address within the caller's code

- the calling function's view:
  - before calling another function we must push as many arguments as the function expects
  - call _function_ _args_
  - after the function returns, the args disappear and are replaced by the return of the function which will also be the stack top
  - after the called function returns all the memory segments are the same as they were before

- the called function's view
  - before starting execution, my argument segment has been initialised with the argument values passed by the caller
  - my local segment has been allocated and initialised to zeroes
  - my static segment was set to static of VM
  - my working stack is empty
  - before returning I must push a value onto the stack
