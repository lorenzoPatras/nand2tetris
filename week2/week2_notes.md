### Week 2 notes
----------------

##### Half adder
- adds without taking into consideration carry_in

| a | b | carry | sum |
|:-:|:-:|:-----:|:---:|
| 0 | 0 |   0   |  0  |
| 0 | 1 |   0   |  1  |
| 1 | 0 |   0   |  1  |
| 1 | 1 |   1   |  0  |

##### Full adder
- offers carry_in bit for add operation

| a | b | c | carry | sum |
|:-:|:-:|:-:|:-----:|:---:|
| 0 | 0 | 0 |   0   |  0  |
| 0 | 0 | 1 |   0   |  1  |
| 0 | 1 | 0 |   0   |  1  |
| 0 | 1 | 1 |   1   |  0  |
| 1 | 0 | 0 |   0   |  1  |
| 1 | 0 | 1 |   1   |  0  |
| 1 | 1 | 0 |   1   |  0  |
| 1 | 1 | 1 |   1   |  1  |

##### Arithmetic Logic Unit
- a unit that performs both arithmetic and logic operations

+ inputs
  * 2 entries on 16 bits (x and y)
  * 4 special entries:
    - zx - 0 on x input
    - nx - negate x input
    - zy - 0 on y input
    - ny - negate y input
  * f - function code (1 for arithmetic, 0 for logic)
  * no - negate output

+ outputs
  * out on 16 bits
  * zr - true if output is 0
  * ng - true is output is negative
