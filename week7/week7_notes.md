Programming process:

  High level language --- compiler ---> VM code ---> VM translator ---> machine language ---> hardware (computer architecture --- digital design ---> CPU, RAM ---> gate logic ---> elementary gates)

A language like C++ is compiled once one very machine it is intended to run.
A language like java is compiled once into virtual machine code. Then whenever it will be run on a machine, it will be compiled again to native code.

Keywords:
  - compilation
  - virtualization
  - VM abstraction
  - stack processing
  - VM implementation
  - pointers
  - programming

### VM abstraction: the stack

#### Memory segment commands
  - push - insert in the top
  - pop - remove from the top
  - push _segment_ i
  - pop _segment_ i

### Arithmetic/Logical commands:
  - add: pop the first two elements of the stack, add them and push the result on the stack
  - neg: negate the topmost value
  - sub: similar to add
  - eq: pop topmost two elements, check if they are equal and push the result on the stack
  - gt
  - lt
  - and
  - or
  - not - not topmost element

|Command|Expression|Return|
|:-----:|:--------:|:----:|
|  add  |   x + y  | int  |
|  sub  |   x - y  | int  |    
|  neg  |     -y   | int  |  
|  eq   |  x == y  | bool |  
|  gt   |  x > y   | bool |  
|  lt   |  x < y   | bool |
|  and  | x and  y | bool |
|  or   |  x or y  | bool |
|  not  |   not y  | bool |

Any arithmetic or logical expression can be expressed and evaluated by applying some sequence of the above operations on a stack

Virtual memory:
  - has multiple segments:
    - local stack - keeps scope vars
    - argument stack - args of functions
    - this stack
    - that stack
    - constant stack - keeps constans
    - static stack - keeps static vars
    - pointer stack
    - temps stack
