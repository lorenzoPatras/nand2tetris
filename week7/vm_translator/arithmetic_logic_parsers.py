from base_parsers import ArithmeticLogicParser


class AddParser(ArithmeticLogicParser):
    def __init__(self, output_file):
        ArithmeticLogicParser.__init__(self, output_file)

    def parse_code_line(self, code_instructions):
        self._copy_stack_top_to_dreg()
        self._jump_to_stack_top()
        self._output_file.write("M=D+M\n")
        self._increment_stack_pointer()


class SubtractParser(ArithmeticLogicParser):
    def __init__(self, output_file):
        ArithmeticLogicParser.__init__(self, output_file)

    def parse_code_line(self, code_instructions):
        self._copy_stack_top_to_dreg()
        self._jump_to_stack_top()
        self._output_file.write("M=M-D\n")
        self._increment_stack_pointer()


class NegationParser(ArithmeticLogicParser):
    def __init__(self, output_file):
        ArithmeticLogicParser.__init__(self, output_file)

    def parse_code_line(self, code_instructions):
        self._put_value_in_dreg("0")
        self._jump_to_stack_top()
        self._output_file.write("M=D-M\n")
        self._increment_stack_pointer()


class ComparatorParser(ArithmeticLogicParser):
    def __init__(self, output_file, comparison):
        ArithmeticLogicParser.__init__(self, output_file)
        self.__entry = 0
        self.__comparison = comparison

    def parse_code_line(self, code_instructions):
        label = self.__generate_label()
        # subtract last two elements from stack
        self._copy_stack_top_to_dreg()
        self._jump_to_stack_top()
        self._output_file.write("D=M-D\n")

        self._output_file.write("@" + label + "\n")
        self._output_file.write("D; J" + self.__comparison + "\n")

        self._output_file.write("@SP\n")
        self._output_file.write("A=M\n")
        self._output_file.write("M=0\n")

        self._output_file.write("@" + label + "_INCREMENT\n")
        self._output_file.write("0;JMP\n")

        self._output_file.write("(" + label + ")\n")
        self._output_file.write("@SP\n")
        self._output_file.write("A=M\n")
        self._output_file.write("M=-1\n")

        self._output_file.write("(" + label + "_INCREMENT)\n")
        self._increment_stack_pointer()
        # generate label

    def __generate_label(self):
        self.__entry += 1
        return self.__comparison + str(self.__entry)


class EqualityParser(ComparatorParser):
    def __init__(self, output_file):
        ComparatorParser.__init__(self, output_file, "EQ")


# checks if SP[top-1] > SP[top]
class GreaterThanParser(ComparatorParser):
    def __init__(self, output_file):
        ComparatorParser.__init__(self, output_file, "GT")


# checks if SP[top-1] < SP[top]
class LessThanParser(ComparatorParser):
    def __init__(self, output_file):
        ComparatorParser.__init__(self, output_file, "LT")


class AndParser(ArithmeticLogicParser):
    def __init__(self, output_file):
        ArithmeticLogicParser.__init__(self, output_file)

    def parse_code_line(self, code_instructions):
        self._copy_stack_top_to_dreg()
        self._jump_to_stack_top()
        self._output_file.write("M=D&M\n")
        self._increment_stack_pointer()


class OrParser(ArithmeticLogicParser):
    def __init__(self, output_file):
        ArithmeticLogicParser.__init__(self, output_file)

    def parse_code_line(self, code_instructions):
        self._copy_stack_top_to_dreg()
        self._jump_to_stack_top()
        self._output_file.write("M=D|M\n")
        self._increment_stack_pointer()


class NotParser(ArithmeticLogicParser):
    def __init__(self, output_file):
        ArithmeticLogicParser.__init__(self, output_file)

    def parse_code_line(self, code_instructions):
        self._jump_to_stack_top()
        self._output_file.write("M=!M\n")
        self._increment_stack_pointer()
