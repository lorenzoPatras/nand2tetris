import os

def get_static_base_name(file_path):
    file_name = os.path.basename(file_path)
    return os.path.splitext(file_name)[0]
