from memory_ops_parsers import *
from arithmetic_logic_parsers import *
from translator_helper import is_code_line
from util import get_static_base_name


class Parser:
    def __init__(self, input_file_path, output_file_path):
        self.__input_file = open(input_file_path, "r")
        self.__output_file = open(output_file_path, "w+")
        self.__parser_factory = ParserFactory(self.__input_file, self.__output_file)

    def parse(self):
        for code_line in self.__input_file:
            if is_code_line(code_line):
                self.__output_file.write("//" + code_line)
                self.__parse_line(code_line)

        self.__output_file.close()

        pass

    def __parse_line(self, code_line):
        words = code_line.split()
        if len(words) is 1:
            selector = words[0]
        elif len(words) is 3:
            selector = words[1]
        else:
            print("Houston we've got a problem... operation is incorrect")
            return

        specific_parser = self.__parser_factory.get_parser(selector)
        specific_parser.parse_code_line(words)


class ParserFactory:
    def __init__(self, input_file, output_file):
        self.__argument_parser = BasicSegmentParser("ARG", output_file)
        self.__local_parser = BasicSegmentParser("LCL", output_file)
        self.__this_parser = BasicSegmentParser("THIS", output_file)
        self.__that_parser = BasicSegmentParser("THAT", output_file)
        self.__constant_parser = ConstantParser(output_file)
        self.__static_parser = StaticParser(get_static_base_name(input_file.name), output_file)
        self.__pointer_parser = PointerParser(output_file)
        self.__temp_parser = TempParser(output_file)
        self.__add_parser = AddParser(output_file)
        self.__subtract_parser = SubtractParser(output_file)
        self.__negation_parser = NegationParser(output_file)
        self.__equality_parser = EqualityParser(output_file)
        self.__greater_than_parser = GreaterThanParser(output_file)
        self.__less_than_parser = LessThanParser(output_file)
        self.__and_parser = AndParser(output_file)
        self.__or_parser = OrParser(output_file)
        self.__not_parser = NotParser(output_file)

        self.memory_access_parsers = {
            "argument": self.__argument_parser,
            "local": self.__local_parser,
            "this": self.__this_parser,
            "that": self.__that_parser,
            "constant": self.__constant_parser,
            "static": self.__static_parser,
            "pointer": self.__pointer_parser,
            "temp": self.__temp_parser,
            "add": self.__add_parser,
            "sub": self.__subtract_parser,
            "neg": self.__negation_parser,
            "eq": self.__equality_parser,
            "gt": self.__greater_than_parser,
            "lt": self.__less_than_parser,
            "and": self.__and_parser,
            "or": self.__or_parser,
            "not": self.__not_parser
        }

    def get_parser(self, selector):
        return self.memory_access_parsers.get(selector, DummyParser())
