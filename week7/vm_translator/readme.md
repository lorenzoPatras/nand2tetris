# Requirement
-------------

Write translation for the following VM instructions:
  1. Arithmetic instructions

| operation       | explication     | programming representation |
| :-------------: | :-------------: | :------------------------: |
| add | add last two elements from stack and persist the result | SP[top-1] = SP[top-1] + SP[top] |
| sub | subtract last two elemend from stack and persist the result | SP[top-1] = SP[top-1] - SP[top] |
| neg | negate value on top of the stack | SP[top] = -SP[top] |
| eq  | check if last two stack values are equal | SP[top-1] = (SP[top-1] == SP[top]) ? -1 : 0 |
| gt  | check if SP[top-1] > SP[top] | SP[top-1] = (SP[top-1] > SP[top]) ? -1 : 0 |
| lt  | check if SP[top-1] < SP[top] | SP[top-1] = (SP[top-1] < SP[top]) ? -1 : 0 |
| and |  | SP[top-1] = SP[top-1] and SP[top] |
| or  |  | SP[top-1] = SP[top-1] or SP[top] |
| not |  | SP[top] = !SP[top] |

  2. Memory access instructions
    - push [segment] i
    - pop [segment] i
    ! segment in [argument, local, this, that, constant, static, pointer, temp]

Stack pointer is mapped on the first register (R0) of the memory. Its value represents the base address of the global stack. Argument is mapped on R1, local is mapped on R2, this is mapped on R3 and that is mapped on R4.

Argument, local, this and that have a similar implementation, the only difference being the address they point at.
 - push _ARG/LCL/THIS/THAT_ i => dest_addr = _ARG/LCL/THIS/THAT_ + i, SP--, \*dest_addr = \*SP (pop value from main stack and push it to segment stack)
 - pop _ARG/LCL/THIS/THAT_ i => dest_addr = _ARG/LCL/THIS/THAT_ + i, \*SP = \*dest_addr, SP++ (pop value from segment stack and push it to main stack)

Constant segment supports only push. We cannot pop a constant. Actually when _push constant i_ is encountered it will be pushed on main stack (\*SP = i, SP++).

Static segment push and pop have to be done on a global space such that static variables will be available to everyone. For this a convention is used, such as _push/pop static i_ will push or pop a value to an address determined by the following rule: @file_name.i. The first such address will be mapped on R16.

Temp segment will push and pop values in the registers 5 to 12.

Pointer segment will push and pop the values stored by THIS and THAT.

There will be left R13, R14 and R15 as general purpose registers.


### Code samples

1. push constant 10
```asm
// put 10 in DReg
@10
D=A
// go to stack top
@SP
A=M
// put DReg in memory segment to which SP points
M=D
// increment stack pointer
@SP
M=M+1
```

2. pop argument 2
```asm
// get base address of ARG
@ARG
D=M
// increment ARG base address with 2
@2
D=D+A
// store the result in a general purpose register
@R15
M=D
// decrement stack pointer and go to stack top
@SP
M=M-1
A=M
// copy stack top to DReg
D=M
// go to destination address previously stored in R15
@R15
A=M
// copy value stored at DReg in memory
M=D
```

3. push that 5
```asm
// get base address of THAT
@THAT
D=M
// increment THAT address with 5
@5
D=D+A
// select the THAT + 5 register
A=D
// copy value from THAT + 5 into DReg
D=M
// go to stack top
@SP
A=M
// copy previously stored value in DReg to stack top
M=D
// increment stack pointer
@SP
M=M+1
```

4. add
```asm
// decrement stack pointer
@SP
M=M-1
// store stack top in DReg
A=M
D=M
// get the second value from stack
@SP
M=M-1
A=M
// store here the result of the add
M=D+M
// increment stack pointer
@SP
M=M+1
```

5. eq
```asm
// get stack top in DReg
@SP
M=M-1
A=M
D=M
// get second stack element
@SP
M=M-1
A=M
// keep in D the difference between SP[top] - SP[top-1]
D=M-D
// if difference is 0 they are equal and jump to EQ1 label
@EQ1
D; JEQ
// else put on stack top 0 (false)
@SP
A=M
M=0
// jump to stack increment
@EQ1_INCREMENT
0;JMP
// if elements are equal put on stack top -1 (true)
(EQ1)
@SP
A=M
M=-1
// increment stack
(EQ1_INCREMENT)
@SP
M=M+1
```

6. and
```asm
// store stack top to DReg
@SP
M=M-1
A=M
D=M
// get second element from stack
@SP
M=M-1
A=M
// put on the new stack top the and result
M=D&M
// increment stack top
@SP
M=M+1
```
