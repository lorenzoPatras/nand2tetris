from base_parsers import BaseParser, MemoryOperationParser


# this parser is used for operations on Local, Argument, This and That operations
class BasicSegmentParser(MemoryOperationParser):
    def __init__(self, segment_name, output_file):
        MemoryOperationParser.__init__(self, output_file)
        self.__segment_name = segment_name

    # dest_address = segment + value - compute destination address
    # *strack_ptr = *dest_address - copy from destination to global stack
    # stack_pointer++ - increment stack ptr
    def _parse_push(self, value):
        self.__compute_segment_address(value)
        # copy value from segment to DReg
        self._output_file.write("A=D\n")
        self._output_file.write("D=M\n")
        self._copy_dreg_to_stack()
        self._increment_stack_pointer()

    # dest_address = segment + value - compute destination address
    # stack_pointer-- - decrement stack ptr
    # *dest_address = *stack_ptr - copy from stack to destination
    def _parse_pop(self, value):
        self.__compute_segment_address(value)
        self.__persist_segment_address()
        self._copy_stack_top_to_dreg()
        # copy value from DReg to segment
        self._output_file.write("@R15\n")
        self._output_file.write("A=M\n")
        self._output_file.write("M=D\n")

    def __compute_segment_address(self, value):
        # address = segment_name + value
        self._output_file.write("@" + self.__segment_name + "\n")
        self._output_file.write("D=M\n")
        self._output_file.write("@" + str(value) + "\n")
        self._output_file.write("D=D+A\n")

    # keep destination in a general purpose reg
    def __persist_segment_address(self):
        self._output_file.write("@R15\n")
        self._output_file.write("M=D\n")


class ConstantParser(MemoryOperationParser):
    def __init__(self, output_file):
        MemoryOperationParser.__init__(self, output_file)

    def _parse_push(self, value):
        self._put_value_in_dreg(str(value))
        self._copy_dreg_to_stack()
        self._increment_stack_pointer()

    def _parse_pop(self, value):  # not popping constants
        pass


class StaticParser(MemoryOperationParser):
    def __init__(self, base_name, output_file):
        MemoryOperationParser.__init__(self, output_file)
        self.__base_name = base_name

    def _parse_push(self, value):
        # compute segment and copy content into DReg
        self._output_file.write("@" + self.__base_name + "." + str(value) + "\n")
        self._output_file.write("D=M\n")

        self._copy_dreg_to_stack()
        self._increment_stack_pointer()

    def _parse_pop(self, value):
        self._copy_stack_top_to_dreg()

        # copute destination and copy into it
        self._output_file.write("@" + self.__base_name + "." + str(value) + "\n")
        self._output_file.write("M=D\n")


class TempParser(MemoryOperationParser):
    def __init__(self, output_file):
        MemoryOperationParser.__init__(self, output_file)
        self.TEMP_BASE_ADDRESS = 5

    def _parse_push(self, value):
        # compute segment and put its content into DReg
        self._output_file.write("@R" + str(self.TEMP_BASE_ADDRESS + int(value)) + "\n")
        self._output_file.write("D=M\n")

        self._copy_dreg_to_stack()
        self._increment_stack_pointer()

    def _parse_pop(self, value):
        self._copy_stack_top_to_dreg()

        # compute destination and copy DReg into it
        self._output_file.write("@R" + str(self.TEMP_BASE_ADDRESS + int(value)) + "\n")
        self._output_file.write("M=D\n")


class PointerParser(MemoryOperationParser):
    def __init__(self, output_file):
        MemoryOperationParser.__init__(self, output_file)

    def _parse_push(self, value):
        # decide between this and that
        if value == "0":
            ptr = "THIS"
        else:
            ptr = "THAT"

        # get value pointed by THIS/THAT
        self._output_file.write("@" + ptr + "\n")
        self._output_file.write("D=M\n")

        self._copy_dreg_to_stack()
        self._increment_stack_pointer()

    def _parse_pop(self, value):
        # decide between this and that
        if value == "0":
            ptr = "THIS"
        else:
            ptr = "THAT"

        self._copy_stack_top_to_dreg()
        # save address in a general purpose register
        self._output_file.write("@" + ptr + "\n")
        self._output_file.write("M=D\n")


class DummyParser(BaseParser):

    def parse_code_line(self, code_line):
        print("this is just a placeholder...")
