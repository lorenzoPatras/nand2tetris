def is_empty_line(line):
    return not line.strip()


def is_comment(line):
    return line.find("//") is 0


def is_code_line(line):
    return not is_empty_line(line) and not is_comment(line)
