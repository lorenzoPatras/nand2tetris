from abc import ABCMeta, abstractmethod

OPERATION_POS = 0
VALUE_POS = 2
OPERATION_PUSH = "push"
OPERATION_POP = "pop"


class BaseParser:
    __metaclass__ = ABCMeta

    @abstractmethod
    def parse_code_line(self, code_instructions):
        pass

    def _decrement_stack_pointer(self):
        self._output_file.write("@SP\n")
        self._output_file.write("M=M-1\n")

    def _increment_stack_pointer(self):
        self._output_file.write("@SP\n")
        self._output_file.write("M=M+1\n")

    def _copy_stack_top_to_dreg(self):
        self._jump_to_stack_top()
        self._output_file.write("D=M\n")

    def _copy_dreg_to_stack(self):
        self._output_file.write("@SP\n")
        self._output_file.write("A=M\n")
        self._output_file.write("M=D\n")

    def _jump_to_stack_top(self):
        self._decrement_stack_pointer()
        self._output_file.write("A=M\n")

    def _put_value_in_dreg(self, value):
        self._output_file.write("@" + value + "\n")
        self._output_file.write("D=A\n")


class MemoryOperationParser(BaseParser):
    __metaclass__ = ABCMeta

    def __init__(self, output_file):
        self._output_file = output_file

    def parse_code_line(self, code_instructions):
        if code_instructions[OPERATION_POS] == OPERATION_PUSH:
            self._parse_push(code_instructions[VALUE_POS])
        elif code_instructions[OPERATION_POS] == OPERATION_POP:
            self._parse_pop(code_instructions[VALUE_POS])
        else:
            print("ERROR: operation not valid: " + code_instructions[OPERATION_POS])

    @abstractmethod
    def _parse_push(self, value):
        pass

    @abstractmethod
    def _parse_pop(self, value):
        pass


class ArithmeticLogicParser(BaseParser):
    def __init__(self, output_file):
        self._output_file = output_file

    def parse_code_line(self, code_instructions):
        pass
