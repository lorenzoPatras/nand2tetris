import argparse
from parser import Parser

parser = argparse.ArgumentParser(description="translate vm code into assembly")
parser.add_argument("--input_file", type=str, help="input file name")
parser.add_argument("--output_file", type=str, help="output file name")

args = parser.parse_args()

if not args.input_file or not args.output_file:
    print("usage: python translator.py --input_file <file_name> --output_file <file_name>")
    exit(0)

parser = Parser(args.input_file, args.output_file)
parser.parse()
