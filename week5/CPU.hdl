// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/05/CPU.hdl

/**
 * The Hack CPU (Central Processing unit), consisting of an ALU,
 * two registers named A and D, and a program counter named PC.
 * The CPU is designed to fetch and execute instructions written in
 * the Hack machine language. In particular, functions as follows:
 * Executes the inputted instruction according to the Hack machine
 * language specification. The D and A in the language specification
 * refer to CPU-resident registers, while M refers to the external
 * memory location addressed by A, i.e. to Memory[A]. The inM input
 * holds the value of this location. If the current instruction needs
 * to write a value to M, the value is placed in outM, the address
 * of the target location is placed in the addressM output, and the
 * writeM control bit is asserted. (When writeM==0, any value may
 * appear in outM). The outM and writeM outputs are combinational:
 * they are affected instantaneously by the execution of the current
 * instruction. The addressM and pc outputs are clocked: although they
 * are affected by the execution of the current instruction, they commit
 * to their new values only in the next time step. If reset==1 then the
 * CPU jumps to address 0 (i.e. pc is set to 0 in next time step) rather
 * than to the address resulting from executing the current instruction.
 */

/*
 * instruction[0..15]
   * instruction[15] = opCode - determines whether it is an A or C instruction
   * instruction[13..14] - unused
   * instruction[12] - determine what to fed into ALU: ARegister or inM (MRegister)
   * instruction[6..11] - control bits for ALU
     * instruction[11] - zx
     * instruction[10] - nx
     * instruction[9] - zy
     * instruction[8] - ny
     * instruction[7] - f
     * instruction[6] - no
   * instruction[3..5] - destination bits
     * instruction[5] - d[2] = load ARegister
     * instruction[4] - d[1] = load DRegister
     * instruction[3] - d[0] = load in memory
   * instruction[0..2] - jump bits
     * instruction[2] - j[2] = jump if negative
     * instruction[1] - j[1] = jump if zero
     * instruction[0] - j[0] = jump if positive
 */

CHIP CPU {

    IN  inM[16],         // M value input  (M = contents of RAM[A])
        instruction[16], // Instruction for execution
        reset;           // Signals whether to re-start the current
                         // program (reset==1) or continue executing
                         // the current program (reset==0).

    OUT outM[16],        // M value output
        writeM,          // Write to M?
        addressM[15],    // Address in data memory (of M)
        pc[15];          // address of next instruction

    PARTS:
    // choose between ALU output and instruction
    Mux16(a = instruction, b = aluInternalOutput, sel = instruction[15], out = entry);

    // define the instruction based on instruction[15] - just for code readability
    Not(in = instruction[15], out = aInstruction);
    Not(in = aInstruction, out = cInstruction);

    // ARegister
    // load into A register if it is an AIntstruction or if it is a CInstruction and instruction[5] is true
    // feed output of ARegister to addressM
    And(a = cInstruction, b = instruction[5], out = aRegTemp);
    Or(a = aRegTemp, b = aInstruction, out = aLoad);
    ARegister(in = entry, load = aLoad, out = aRegisterOut, out[0..14] = addressM);

    // DRegister
    // load into DRegister if CInstruction and dLoad(instruction[4]) is true
    And(a = cInstruction, b = instruction[4], out = dLoad);
    DRegister(in = aluInternalOutput, load = dLoad, out = dRegisterOut);

    // ALU
    // select the second entry for ALU between ARegister and inM
    Mux16(a = aRegisterOut, b = inM, sel = instruction[12], out = secondOperand);

    // set control bits for ALU
    ALU(x = dRegisterOut, y = secondOperand, zx = instruction[11],
        nx = instruction[10], zy = instruction[9], ny = instruction[8],
        f = instruction[7], no = instruction[6], out = aluInternalOutput,
        out = outM, zr = zeroFlag, ng = negativeFlag);

    // write memory mRegister
    And(a = cInstruction, b = instruction[3], out = writeM);

    // program counter
    // determine jump condition
    Or(a = zeroFlag , b = negativeFlag, out = lessFlag);
    Not(in = lessFlag, out = positiveFlag);

    And(a = positiveFlag, b = instruction[0], out = jumpPositive);
    And(a = zeroFlag, b = instruction[1], out = jumpZero);
    And(a = negativeFlag, b = instruction[2], out = jumpNegative);

    Or(a = jumpPositive, b = jumpZero, out = jumpGE);
    Or(a = jumpGE, b = jumpNegative, out = jumpTemp);
    And(a = jumpTemp, b = cInstruction, out = jump);

    PC(in = aRegisterOut, load = jump, inc = true, reset = reset, out[0..14] = pc);
}
