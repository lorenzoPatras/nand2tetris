### Week 1 request
--------------------

1. Create HDL components for Not, And, Or, Xor based on Nand gate.

2. Create HDL components for Mux and DMux

3. Create HDL components for And, Not, Or on 16 bits

4. Create HDL components for some custom components that will be helpful later in constructing the computer like Or8Way (or between 8 bits), Mux4Way16 (mux with 4 wntries and 16 bits BUS), Mux8Way16, DMux4Way16, DMux8Way16
