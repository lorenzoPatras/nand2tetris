### Week 1 notes
----------------

#### Basic logic operations


###### Not
| in | out |
|:--:|:---:|
| 0  |  1  |
| 1  |  0  |

###### And
| a | b | out |
|:-:|:-:|:---:|
| 0 | 0 |  0  |
| 0 | 1 |  0  |
| 1 | 0 |  0  |
| 1 | 1 |  1  |

###### Or
| a | b | out |
|:-:|:-:|:---:|
| 0 | 0 |  0  |
| 0 | 1 |  1  |
| 1 | 0 |  1  |
| 1 | 1 |  1  |

###### Xor
| a | b | out |
|:-:|:-:|:---:|
| 0 | 0 |  0  |
| 0 | 1 |  1  |
| 1 | 0 |  1  |
| 1 | 1 |  0  |

#### Complex gates

###### Mux
- based on sel input, output takes value of input a or b

| a | b |  sel  |  out  |
|:-:|:-:|:-----:|:-----:|
| 0 | 0 |   0   |   0   |
| 0 | 0 |   1   |   0   |
| 0 | 1 |   0   |   0   |
| 0 | 1 |   1   |   1   |
| 1 | 0 |   0   |   1   |
| 1 | 0 |   1   |   0   |
| 1 | 1 |   0   |   1   |
| 1 | 1 |   1   |   1   |


###### DMux
- based on sel input, input is sent to either a or b output

| in | sel | a | b |
|:--:|:---:|:-:|:-:|
| 0  |  0  | 0 | 0 |
| 0  |  1  | 0 | 0 |
| 1  |  0  | 1 | 0 |
| 1  |  1  | 0 | 1 |
